import 'package:flutter_test/flutter_test.dart';
import 'package:std_labs/src/domain/validator.dart';

main() {
  group('InputValidator', () {
    final validator = InputValidator();

    var testCases = [
      ['', ValidationResult.ERROR_EMPTY],
      ['Hello! How are you?', ValidationResult.ERROR_WRONG_CHARACTERS],
      ['Привет! Как дела?', ValidationResult.ERROR_WRONG_CHARACTERS],
      ['1 2 3 4,5,6,7, 8, 9.', ValidationResult.OK],
      ['Lorem ипсум 11 dolor сит 005 amet.', ValidationResult.OK]
    ];

    testCases.forEach((testCase) {
      test('returns ${testCase[1].toString()} for input \"${testCase[0].toString()}\"', () {
        var source = testCase[0];
        var expected = testCase[1];
        var actual = validator.validate(source);

        expect(actual, expected);
      });
    });
  });
}
