import 'package:flutter_test/flutter_test.dart';
import 'package:std_labs/src/domain/transformer.dart';
import 'package:std_labs/src/domain/validator.dart';

void main() {
  group('Replace if valid:', () {
    var testCases = [
      ['#', '10', '##'],
      ['#', '##', null],
      ['#', '11', '11'],
      ['#', 'hello', 'hello'],
      ['#', '001, 002, 003, 004, 005, 006, 007, 008, 009, 010, 011', '###, ###, ###, ###, ###, ###, ###, ###, ###, ###, 011'],
      ['#', 'Валидная 1 входная 0000000001 строка.', 'Валидная # входная ########## строка.'],
      ['#', 'Невалидная 1 строчка закралась сюда!', null],
      ['*', '10', '**'],
      ['*', '**', null],
      ['*', '11', '11'],
      ['*', 'hello', 'hello'],
      ['*', '001, 002, 003, 004, 005, 006, 007, 008, 009, 010, 011', '***, ***, ***, ***, ***, ***, ***, ***, ***, ***, 011'],
      ['*', 'Валидная 1 входная 0000000001 строка.', 'Валидная * входная ********** строка.'],
      ['*', 'Невалидная 1 строчка закралась сюда!', null],
    ];

    testCases.forEach((testCase) {
      test('mask ${testCase[0]} from ${testCase[1]} to ${testCase[2]}', () {
        final validator = InputValidator();
        final transformer = SmallNumberMasker(mask: testCase[0]);

        var source = testCase[1];
        var expected = testCase[2];
        var actual = validator.validate(source) == ValidationResult.OK ? transformer.process(source) : null;

        expect(actual, expected);
      });
    });
  });
}