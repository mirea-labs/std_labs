import 'package:flutter_test/flutter_test.dart';
import 'package:std_labs/src/domain/transformer.dart';

void main() {
  group('SmallNumberMasker', () {
    var testCases = [
      ['#', '10', '##'],
      ['#', '11', '11'],
      ['#', 'hello', 'hello'],
      ['*', 'Hello! This is 1, and 2.', 'Hello! This is *, and *.'],
      ['#', 'Hello! This is 0001, and 34.', 'Hello! This is ####, and 34.'],
      ['#', '001, 002, 003, 004, 005, 006, 007, 008, 009, 010, 011', '###, ###, ###, ###, ###, ###, ###, ###, ###, ###, 011']
    ];

    testCases.forEach((testCase) {
      test('replaces with mask ${testCase[0]} from ${testCase[1]} to ${testCase[2]}', () {
        final transformer = SmallNumberMasker(mask: testCase[0]);

        var source = testCase[1];
        var expected = testCase[2];
        var actual = transformer.process(source);

        expect(actual, expected);
      });
    });
  });
}
