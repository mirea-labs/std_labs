import 'package:flutter_driver/flutter_driver.dart';
import 'package:test/test.dart';

void main() {
  group('Приложение', () {
    final inputFinder = find.byValueKey('field_input');
    final transformerFinder = find.byValueKey('field_transformer');
    final replaceButtonFinder = find.byValueKey('button_replace');
    final resultFinder = find.byValueKey('text_result');
    final resultTitleFinder = find.byValueKey('text_title_result');

    FlutterDriver driver;

    // Connect to the Flutter driver before running any tests.
    setUpAll(() async {
      driver = await FlutterDriver.connect();
    });

    // Close the connection to the driver after the tests have completed.
    tearDownAll(() async {
      if (driver != null) {
        driver.close();
      }
    });

    test('выполняет замену корректно при корректных входных данных и выбранном преобразователе (*)', () async {
      // 1. Ввести текст
      await driver.tap(inputFinder);
      await driver.enterText('Это 1 корректная,2 строка');

      // 2. Выбрать преобразователь
      await driver.tap(transformerFinder);
      await driver.tap(find.text('Замена на * чисел [1, 10]'));

      // 3. Нажать на кнопку "Заменить"
      await driver.tap(replaceButtonFinder);

      // Выводится преобразованная строка
      expect(await driver.getText(resultTitleFinder), '2. Проверьте результат');
      expect(await driver.getText(resultFinder), 'Это * корректная,* строка');
    });

    test('выполняет замену корректно при корректных входных данных и выбранном преобразователе (#)', () async {
      // 1. Ввести текст
      await driver.tap(inputFinder);
      await driver.enterText('Это 1 корректная,2 строка');

      // 2. Выбрать преобразователь
      await driver.tap(transformerFinder);
      await driver.tap(find.text('Замена на # чисел [1, 10]'));

      // 3. Нажать на кнопку "Заменить"
      await driver.tap(replaceButtonFinder);

      // Выводится преобразованная строка
      expect(await driver.getText(resultTitleFinder), '2. Проверьте результат');
      expect(await driver.getText(resultFinder), 'Это # корректная,# строка');
    });

    test('не выполняет замену при некорректных входных данных и выбранном преобразователе', () async {
      // 1. Ввести текст
      await driver.tap(inputFinder);
      await driver.enterText('Это 1 некорректная строка!!!!11!1!1');

      // 2. Выбрать преобразователь
      await driver.tap(transformerFinder);
      await driver.tap(find.text('Замена на * чисел [1, 10]'));

      // 3. Нажать на кнопку "Заменить"
      await driver.tap(replaceButtonFinder);

      // Содержание карточки результата не изменилось с момента запуска
      expect(await driver.getText(resultTitleFinder), '2. Здесь будет результат');
    });

    test('не выполняет замену при пустой входной строке и выбранном преобразователе', () async {
      // 1. Ввести текст
      await driver.tap(inputFinder);
      await driver.enterText('');

      // 2. Выбрать преобразователь
      await driver.tap(transformerFinder);
      await driver.tap(find.text('Замена на * чисел [1, 10]'));

      // 3. Нажать на кнопку "Заменить"
      await driver.tap(replaceButtonFinder);

      // Содержание карточки результата не изменилось с момента запуска
      expect(await driver.getText(resultTitleFinder), '2. Здесь будет результат');
    });
  });
}
