import 'package:flutter_driver/driver_extension.dart';
import 'package:std_labs/src/main.dart' as app;

void main() {
  enableFlutterDriverExtension();
  app.main();
}
