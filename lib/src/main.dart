import 'package:flutter/material.dart';
import 'package:std_labs/src/domain/transformer.dart';
import 'package:std_labs/src/domain/validator.dart';
import 'package:std_labs/src/presentation/converter/validation_result_converter.dart';
import 'package:std_labs/src/presentation/screens/transformer_route.dart';
import 'package:std_labs/src/presentation/transformer_descriptor.dart';

void main() => runApp(SoftwareTestingAndDebuggingApp());

class SoftwareTestingAndDebuggingApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Software Testing and Debugging Lab',
      theme: ThemeData(
        primarySwatch: Colors.green,
        accentColor: Colors.deepOrange
      ),
      home: TransformerRoute(
        inputValidator: InputValidator(),
        validationResultConverter: ValidationResultConverter(),
        transformerDescroptors: <TransformerDescriptor>[
          TransformerDescriptor(
            description: 'Замена на * чисел [1, 10]',
            transformer: SmallNumberMasker(mask: '*'),
          ),
          TransformerDescriptor(
            description: 'Замена на # чисел [1, 10]',
            transformer: SmallNumberMasker(mask: '#'),
          ),
        ],
      ),
    );
  }
}
