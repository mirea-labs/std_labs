import 'package:meta/meta.dart';

class Transformer<T> {
  final bool Function(T) predicate;
  final T Function(T) transformation;

  Transformer({@required this.predicate, @required this.transformation});

  T process(T source) {
    if (predicate(source)) {
      return transformation(source);
    } else {
      return source;
    }
  }
}

class SmallNumberMasker extends Transformer<String> {
  final String mask;

  SmallNumberMasker({@required this.mask})
      : super(
          predicate: (_) => true,
          transformation: (String input) {
            var regex = RegExp(r"\d+");
            return input.replaceAllMapped(
              regex,
              (match) {
                var matched = match.group(0);
                int parsed = int.parse(matched.replaceAll("^0+", ""));

                if (parsed >= 1 && parsed <= 10) {
                  var replacement = matched.replaceAll(RegExp(r"."), mask);
                  return "$replacement";
                } else {
                  return matched;
                }
              },
            );
          },
        );
}
