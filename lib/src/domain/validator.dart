import 'package:flutter/material.dart';

@immutable
class InputValidator {
  ValidationResult validate(String input) {
    bool isEmpty = input.isEmpty;
    bool containsWrongCharacters = input.contains(RegExp(r'[^\s\wА-Яа-я\d.,]'));

    if (isEmpty) {
      return ValidationResult.ERROR_EMPTY;
    }

    if (containsWrongCharacters) {
      return ValidationResult.ERROR_WRONG_CHARACTERS;
    }

    return ValidationResult.OK;
  }
}

enum ValidationResult {
  OK, ERROR_EMPTY, ERROR_WRONG_CHARACTERS
}