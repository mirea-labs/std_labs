import 'package:std_labs/src/domain/converter.dart';
import 'package:std_labs/src/domain/validator.dart';

class ValidationResultConverter extends OneWayConverter<ValidationResult, String> {
  @override
  String convert(ValidationResult source) {
    switch (source) {
      case ValidationResult.OK:
        return null;
      case ValidationResult.ERROR_EMPTY:
        return 'Введите предложение';
      case ValidationResult.ERROR_WRONG_CHARACTERS:
        return 'Может содержать только английские и русские буквы, цифры, точки и запятые';
    }

    throw Exception();
  }
}
