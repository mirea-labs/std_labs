import 'package:flutter/material.dart';
import 'package:std_labs/src/domain/converter.dart';
import 'package:std_labs/src/domain/validator.dart';
import 'package:std_labs/src/presentation/transformer_descriptor.dart';

class TransformerRoute extends StatefulWidget {
  final InputValidator inputValidator;
  final OneWayConverter<ValidationResult, String> validationResultConverter;
  final List<TransformerDescriptor> transformerDescroptors;

  TransformerRoute({
    @required this.transformerDescroptors,
    @required this.inputValidator,
    @required this.validationResultConverter,
  });

  @override
  State<StatefulWidget> createState() => _TransformerRouteState(
      transformers: transformerDescroptors, inputValidator: inputValidator, validationResultConverter: validationResultConverter);
}

class _TransformerRouteState extends State<TransformerRoute> {
  final _formKey = GlobalKey<FormState>();
  final List<TransformerDescriptor> transformers;
  final InputValidator inputValidator;
  final OneWayConverter<ValidationResult, String> validationResultConverter;
  TransformerDescriptor _selectedTransformer;
  String _inputText;
  String _transformedText;

  _TransformerRouteState({
    @required this.transformers,
    @required this.inputValidator,
    @required this.validationResultConverter,
  }) : _selectedTransformer = transformers[0];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Software Testing Lab'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.help),
            onPressed: () {},
          )
        ],
      ),
      body: Builder(
        builder: (BuildContext context) => ListView(
          children: <Widget>[
            _form(context),
            _result(),
          ],
        ),
      ),
    );
  }

  String _getErrorForMask(TransformerDescriptor input) {
    if (input == null) {
      return 'Выберите правило преобразования';
    }

    return null;
  }

  void _processForm(BuildContext context) {
    FocusScope.of(context).requestFocus(new FocusNode());

    bool isFormValid = _formKey.currentState.validate();

    if (!isFormValid) {
      setState(() {
        _inputText = _transformedText = null;
      });
      final snackBar = SnackBar(content: Text('Исправьте ошибки и попробуйте еще раз'));
      Scaffold.of(context).showSnackBar(snackBar);
      return;
    }

    setState(() {
      _transformedText = _selectedTransformer.transformer.process(_inputText);
    });
  }

  Widget _form(BuildContext scaffoldContext) {
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(16),
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                '1. Заполните форму',
                style: Theme.of(context).textTheme.headline5,
              ),
              TextFormField(
                key: Key('field_input'),
                validator: (input) => validationResultConverter.convert(inputValidator.validate(input)),
                keyboardType: TextInputType.multiline,
                maxLines: null,
                decoration: InputDecoration(
                  labelText: 'Входные данные',
                  errorMaxLines: 2,
                  icon: Icon(Icons.input),
                ),
                onChanged: (source) {
                  setState(() {
                    _inputText = source;
                  });
                },
              ),
              DropdownButtonFormField<TransformerDescriptor>(
                key: Key('field_transformer'),
                validator: (TransformerDescriptor input) => _getErrorForMask(input),
                onChanged: (mask) {
                  setState(() {
                    _selectedTransformer = mask;
                  });
                },
                decoration: InputDecoration(
                  icon: Icon(Icons.details),
                  labelText: 'Правило преобразования',
                ),
                value: _selectedTransformer,
                items: transformers
                    .map(
                      (mask) => DropdownMenuItem<TransformerDescriptor>(
                        value: mask,
                        child: Text('${mask.description}'),
                      ),
                    )
                    .toList(),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  RaisedButton(
                    key: Key('button_replace'),
                    child: Text('Заменить'),
                    onPressed: () => _processForm(scaffoldContext),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  _result() {
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Text(
              _transformedText != null ? '2. Проверьте результат' : '2. Здесь будет результат',
              key: Key('text_title_result'),
              style: Theme.of(context).textTheme.headline5,
            ),
            Visibility(
              child: Padding(
                padding: const EdgeInsets.only(top: 16.0),
                child: Text(
                  _transformedText != null ? _transformedText : '',
                  key: Key('text_result'),
                  style: Theme.of(context).textTheme.headline6,
                ),
              ),
              visible: _transformedText != null,
            )
          ],
        ),
      ),
    );
  }
}
