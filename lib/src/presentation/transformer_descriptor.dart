import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:std_labs/src/domain/transformer.dart';

class TransformerDescriptor {
  final String description;
  final Transformer transformer;

  const TransformerDescriptor({
    @required this.description,
    @required this.transformer,
  });
}
